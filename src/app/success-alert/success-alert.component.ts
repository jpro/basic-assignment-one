import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-success-alert',
  template: `
    <div class="alert alert-success">
      <strong>Success!</strong>
      {{ successMessage }}
    </div>
  `,
  styleUrls: ['./success-alert.component.css']
})
export class SuccessAlertComponent implements OnInit {
  successMessage:string;

  constructor() {}

  ngOnInit() {
    this.successMessage = "Your data has been successfully saved onto our servers.";
  }

}
