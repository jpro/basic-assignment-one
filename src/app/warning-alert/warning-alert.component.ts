import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-warning-alert',
  templateUrl: './warning-alert.component.html',
  styleUrls: ['./warning-alert.component.css']
})
export class WarningAlertComponent implements OnInit {
  warningMessage:string;

  constructor() {}

  ngOnInit() {
    this.warningMessage = `Should you repeat this offense, we will send the Internet Police
      over to your house (localhost or 127.0.0.1)...`;
  }

}
