import { BasicsAssignmentOnePage } from './app.po';

describe('basics-assignment-one App', () => {
  let page: BasicsAssignmentOnePage;

  beforeEach(() => {
    page = new BasicsAssignmentOnePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
